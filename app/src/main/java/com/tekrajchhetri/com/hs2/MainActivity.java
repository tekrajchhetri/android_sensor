package com.tekrajchhetri.com.hs2;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Point;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

import static java.lang.Math.abs;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    SensorManager sensorManager;
    ConstraintLayout.LayoutParams params;
    Sensor sensor;
    ImageView imageView;
    Sensor rotationSensor;
    int screenwidth;
    int getScreenheight;
    private Handler handler;
    private Timer timer;
    float degree = 0;

    private Button button;
    float startposition = 0;
    int collisionLL = 0;
    int collisionLR = 0;
    int collisionRL = 0;
    int collisionRR = 0;
    ConstraintLayout layouts;
    int isLeft = 0;//0 denotes left and 1 denotes right
    float yAccel = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
       // rotationSensor = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
          layouts = findViewById(R.id.layouts);
        imageView = findViewById(R.id.ball_game);

        button = findViewById(R.id.button);


        ConstraintLayout layouts = findViewById(R.id.layouts);
       int left = layouts.getLeft();
    }


    private void rotate(int howmuch){
         degree += howmuch;
         imageView.setRotation(degree);

    }


    @Override
    public void onSensorChanged(SensorEvent event) {


        //get whether reverlandscape or not
        int rotation = getWindowManager().getDefaultDisplay().getRotation();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        Log.d("Rotation",(rotation == Surface.ROTATION_270)+"");
         yAccel = 0;

        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER){
            yAccel = event.values[1];
        }



        Log.d("xAccel-te",height+">>"+rotation+">>>"+yAccel);
        int btnwt = button.getWidth();
       float btnloc = button.getX();

        int locations[] = new int[2];
        int locationball[] = new int[2];

        button.getLocationOnScreen(locations);
        imageView.getLocationOnScreen(locationball);


        float current = imageView.getX();
        startposition = current;
        //fix the reverse landscape issue
        if (rotation == 3){
            yAccel = -1 * yAccel;
        }else if (rotation == 1){
            yAccel = yAccel;
        }

        //o.5 taken to make some buffer
        if ( yAccel > 0.5){//Left to Right

            //collision Right Right RR if greater than R
            if ((current + 60) > layouts.getRight()){
                //if right end is reached then stop moving right
                //update to right - 1 i.e. inside viewport
                if (collisionRR == 0){
//                    Log.d("greater",(current+60)+"}"+layouts.getRight());
                    imageView.setX(layouts.getRight()-imageView.getWidth()-50);
                    collisionRR = 1;
                }
            }else{
                //move to right otherwise and makesure in the boundary
                if ((current + 60) < layouts.getRight()-imageView.getWidth()){
                    //check if Right of left obstacle is reached
                    //collision LR
                    //otherwise update
                    if (isLeft==0 && (locationball[0]+imageView.getWidth()) < (locations[0])){
                        Log.d("greater",locations[0]+"}"+locationball[0]);
                        imageView.setX(current+60);
                    }else if (isLeft==1){ //we are on right side
                        //so move freely as we're checking the rightmost collision above
                        imageView.setX(current+60);

                    }

                }

            }

        }
        //0.5 taken to give some little buffer
        //so that it will not flow i.e. be sensitive
        else if  (yAccel < -0.5){ //Right to Left Rotation

            if (current - 60 < 0){
                Log.d("current_position",current+"NLect");
                //stop ball if left Left Left collision occur
                if (collisionLL == 0){
                   // Log.d("Button_left",(button.getLeft()-imageView.getWidth()-60)+"leftbtn");
                    imageView.setX(60);
                    collisionLL = 1;
                }

            }else {
                Log.d("current_position",current+"NLect");
                //collisionRL;
                if (isLeft == 0){//we are on left side just move as we've already checked collision
                    imageView.setX(current - 60);
                }else if (isLeft == 1){
                    //we are on the right side
                    //so check RL collision i.e. left collision from right side
                    if (locationball[0] > (locations[0]+imageView.getWidth()-60)){
                        imageView.setX(current - 60);
                    }


                }

            }

        }



    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        sensorManager.registerListener(this,sensor,SensorManager.SENSOR_DELAY_NORMAL);
        //sensorManager.registerListener(this,rotationSensor,SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        sensorManager.unregisterListener(this);
    }

    public void jump(View view) throws InterruptedException {
        //get current y position i.e. above line
        Log.d("yaccel",yAccel+"");
        Log.d("yaccel",isLeft+" Left");
        float y = imageView.getY();
        //isLeft = isLeft == 0? 1 :0;

        //check if left then move right
        if (isLeft == 0){//jump right i.e. we are on the left side
            //make jump on the same if device is not tilted and bring down to same position
            if (yAccel > 0.5){ //device is tilted towards right movement
                   isLeft = 1;
//                imageView.setY(66);//move up 66 points
//                imageView.setX(layouts.getRight()-imageView.getWidth()); //move right
                ObjectAnimator anim = ObjectAnimator.ofFloat(imageView,"y",76f);
                imageView.setX(layouts.getRight()-imageView.getWidth());
               // ObjectAnimator animx = ObjectAnimator.ofFloat(imageView,"x",layouts.getRight()-imageView.getWidth()); not working as expected
                ObjectAnimator anims = ObjectAnimator.ofFloat(imageView,"y",y);
                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.playSequentially(anim,anims);
                animatorSet.setDuration(400);
                animatorSet.start();
            }else{

                //we are on left but device not tilted to right so just jump don't cross the obstacle
                ObjectAnimator anim = ObjectAnimator.ofFloat(imageView,"y",66f);
                ObjectAnimator anims = ObjectAnimator.ofFloat(imageView,"y",y);
                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.playSequentially(anim,anims);
                animatorSet.setDuration(400);
                animatorSet.start();

                Log.d("yaccel","******justjump"+imageView.getY());
//                imageView.setY( 66);//move up 66 points
//

                Log.d("yaccel","Newjustjump"+imageView.getY());
            }


        }else{ //currently isleft is 1 i.e we are on right

            if (yAccel < -0.5) { //device is tilted towards left  so jump to left of obstacle on click
                isLeft = 0;
                ObjectAnimator animl = ObjectAnimator.ofFloat(imageView,"y",66f);
                imageView.setX(layouts.getLeft()+60); //move left
               // ObjectAnimator animlx = ObjectAnimator.ofFloat(imageView,"x",layouts.getLeft()+60); not working as expected
                ObjectAnimator animls = ObjectAnimator.ofFloat(imageView,"y",y);
                AnimatorSet animatorSetsl = new AnimatorSet();
                animatorSetsl.playSequentially(animl,animls);
                animatorSetsl.setDuration(400);
                animatorSetsl.start();
            }else{
                //we are on right but device not tilted to left so just jump don't cross the obstacle
                ObjectAnimator anims= ObjectAnimator.ofFloat(imageView,"y",66f);
                ObjectAnimator animss = ObjectAnimator.ofFloat(imageView,"y",y);
                AnimatorSet animatorSets = new AnimatorSet();
                animatorSets.playSequentially(anims,animss);
                animatorSets.setDuration(400);
                animatorSets.start();

            }
        }

//        imageView.setY(y);//move down to same y above line
    }
}
